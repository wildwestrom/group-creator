(ns wildwestrom.grouper
  (:gen-class)
  (:require [clojure.java.io :as io]
            [clojure.data.csv :as csv]
            [ubergraph.core :as uber]
            [clojure.math.combinatorics :as combo]))

;;;; read/create data

(def csv-raw
  (with-open [reader (io/reader "resources/Assessment_form_Responses.csv")]
    (doall
      (csv/read-csv reader))))

(defn csv-data->maps [csv-data]
  (map zipmap
       (->> (first csv-data) ;; First row is the header
            (map keyword) ;; Drop if you want string keys instead
            repeat)
       (rest csv-data)))

(defn csv-data [csv-data]
  (->> csv-data
       rest
       (map rest)
       (map rest)
       (map vec)
       vec))

(defn answers-to-vals
  [data]
  (cond
    (or (= "Yes" data)
        (= "yes" data))   5
    (or (= "Maybe" data)
        (= "maybe" data)) 10
    (or (= "No" data)
        (= "no" data))    15
    (or (= "Me" data)
        (= "me" data))    0
    :else                 :error))

(defn csv-head [csv-data]
  (first csv-data))


(def data-head (seq (csv-head csv-raw)))

(def respondents
  (set (map keyword (remove #(or (= "respondent" %)
                                 (= "numberofgroups" %)) (vec data-head)))))

(def data-map (csv-data->maps csv-raw))

(def data-range (range (count respondents)))

(def data
  (into {} (for [n data-range]
             (zipmap (vector (keyword (:respondent (nth data-map n))))
                     (vector (dissoc (nth data-map n) :respondent))))))

(def pairs (combo/permuted-combinations respondents 2))

(def pair-count (count pairs))

(defn pair-value
  [key-1 key-2]
  (->> [key-1]
       (select-keys (get data key-2))
       first
       last
       answers-to-vals))

(defn pair-weight
  [pair]
  (* (pair-value (nth pair 0)(nth pair 1))
     (pair-value (nth pair 1)(nth pair 0))))

(defn uber-edge
  [pair]
  (vector (nth pair 0)
          (nth pair 1)
          {:len (pair-weight pair)}))

(defn edges
  [pairs]
  (map #(uber-edge %) pairs))

(apply uber/graph (edges pairs))

(uber/viz-graph (apply uber/graph (edges pairs)) {:save {:filename "resources/graph.png" :format :png} :layout :neato})

;;(uber/viz-graph (apply uber/graph (edges pairs)) {:layout :neato})
