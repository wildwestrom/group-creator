# Group Creator

Applies a group-creation algorithm based on responses from people on whether they want be in a group with another person.

## Installation

Download from https://github.com/wildwestrom/grouper

## Usage

FIXME: explanation

Run the project directly:

    $ clojure -M -m wildwestrom.grouper

Run the project's tests (they'll fail until you edit them):

    $ clojure -M:test:runner

Build an uberjar:

    $ clojure -M:uberjar

Run that uberjar:

    $ java -jar grouper.jar

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2021 Main

Distributed under the GNU General Public License v3.0.
